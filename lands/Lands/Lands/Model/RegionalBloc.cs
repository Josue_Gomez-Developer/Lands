﻿namespace Lands.Model
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class RegionalBloc
    {
        [JsonProperty(PropertyName = "acronym")]
        public string Acronym { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }
    }
}
